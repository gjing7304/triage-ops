# frozen_string_literal: true

require_relative 'milestone'

module Triage
  class MergeRequest
    attr_reader :attributes

    def initialize(project_id, iid)
      @project_id = project_id
      @iid = iid
      @attributes = Triage.api_client.merge_request(project_id, iid)
    end

    def label_names
      attributes['labels']
    end
  end
end
