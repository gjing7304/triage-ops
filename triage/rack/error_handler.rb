# frozen_string_literal: true

require 'rack'

require_relative '../triage/sentry'

module Triage
  module Rack
    ErrorHandler = Struct.new(:app) do
      def call(env)
        logger = env[::Rack::RACK_LOGGER]
        app.call(env)
      rescue StandardError => e
        logger.error(e)
        Raven.capture_exception(e)
        ::Rack::Response.new([JSON.dump(status: :error, error: e.class, message: e.message)], 500).finish
      end
    end
  end
end
