# frozen_string_literal: true

require 'mini_cache'
require 'rack'
require 'sucker_punch'

module Triage
  module Rack
    class Dashboard
      STATS_EXPIRES_IN = 10

      def call(_env)
        stats_json = cache.get_or_set(:stats_json, expires_in: STATS_EXPIRES_IN) do
          JSON.pretty_generate(SuckerPunch::Queue.stats)
        end

        ::Rack::Response.new([stats_json], 200).finish
      end

      private

      def cache
        @cache ||= MiniCache::Store.new
      end
    end
  end
end
