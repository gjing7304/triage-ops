# frozen_string_literal: true

require_relative '../../triage/processor'
require_relative '../../triage/devops_labels_validator'
require_relative '../../job/devops_labels_nudger_job'

module Triage
  module Workflow
    class DevopsLabelsNudger < Processor
      react_to 'issue.open'

      FIVE_MINUTES = 300

      def applicable?
        !DevopsLabelsValidator.new(event).labels_set?
      end

      def process
        DevopsLabelsNudgerJob.perform_in(FIVE_MINUTES, event)
      end
    end
  end
end
