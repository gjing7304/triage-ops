bplist00�
X$versionY$archiverT$topX$objects ��_NSKeyedArchiver�	Troot��+1259=>CGKU$null�_attributedStringData]dataPersisterV$class����WNS.dataO��# frozen_string_literal: true

require_relative 'community_processor'

require_relative '../../strings/thanks'

module Triage
  class ThankContribution < CommunityProcessor
    include Strings::Thanks

   GitLab.org / Professional Services Automation / Tools / Migration / Congregate incoming+f788b07907f3b3a16b53556a3c2cb09c@incoming.gitlab.com

    react_to 'merge_request.open'

    def applicable?
      event.wider_community_author? &&
        (event.from_gitlab_org? || event.from_www_gitlab_com?) &&
        EXCLUDED_PATH_PREFIXES.none? { |prefix| event.project_path_with_namespace.start_with?(prefix) }
    end

    def process
      post_thank_you_message
    end

    def documentation
      <<~TEXT
        This processor posts a thank you message to www.gitlab.com merge requests created by community contributors.
      TEXT
    end

    private

    def post_thank_you_message
      thank_you_message = project_thanks
      body = format(thank_you_message, author_username: event.event_actor_username)
      add_comment(body, append_source_link: true)
    end

    def project_thanks
      case event.project_id
      when GITLAB_RUNNER_PROJECT_ID
        runner_thanks
      when WWW_GITLAB_COM_PROJECT_ID
        www_gitlab_com_thanks
      when GITLAB_FOSS_PROJECT_ID
        gitlab_foss_thanks
      else
        default_thanks
      end
    end

    def default_thanks
      <<~MARKDOWN.chomp
        #{intro_thanks}

        #{READY_FOR_REVIEW}

        #{REQUEST_HELP}

        #{GROUP_LABEL}

        #{SIGNOFF_THANKS}
      MARKDOWN
    end

    def gitlab_foss_thanks
      <<~MARKDOWN.chomp
        #{GITLAB_FOSS_BODY}

        #{SIGNOFF_THANKS}
      MARKDOWN
    end

    def runner_thanks
      <<~MARKDOWN.chomp
        #{intro_thanks}

        #{READY_FOR_REVIEW}

        #{REQUEST_HELP}

        #{RUNNER_BODY}

        #{SIGNOFF_THANKS}
      MARKDOWN
    end

    def www_gitlab_com_thanks
      <<~MARKDOWN.chomp
        #{intro_thanks}

        #{READY_FOR_REVIEW}

        #{WWW_GITLAB_COM_BODY}

        #{SIGNOFF_THANKS}
      MARKDOWN
    end

    def intro_thanks
      event.from_community_fork? ? INTRO_THANKS_FOR_COMMUNITY_FORK : INTRO_THANKS_DEFAULT
    end
  end
end
*� *� *�
 ��Z$classnameX$classes]NSMutableData�]NSMutableDataVNSDataXNSObject� !"#$%&'()*_accumulatedDataSize_objectIdentifierWallURLs_identifierToDataDictionary_cacheDirectoryURL �
�����,-./0WNS.base[NS.relative� ��_�file:///private/var/mobile/Containers/Data/Application/E4DD2610-5509-4B22-881A-7545F5E84F0F/tmp/pasteboardDataPersister/7A9D319F-D946-4D07-AE8C-8D626362A97E�34UNSURL�3�678ZNS.objects��	�:;^NSMutableArray�:<WNSArray_$6B418452-E949-4525-A179-CF10B3157D58�?6@ABWNS.keys����DE_NSMutableDictionary�DF\NSDictionary�HI_ICDataPersister�J_ICDataPersister�LM_ICNotePasteboardData�N_ICNotePasteboardData    $ ) 2 7 I L Q S e k r � � � � � � � �	r	t	y	�	�	�	�	�	�	�	�	�	�	�

,
.
0
2
4
6
8
?
G
S
U
W
Y
�
�-19`gopqsx����������             O              