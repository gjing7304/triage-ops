# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../../lib/www_gitlab_com'

module Triage
  class TeamLabelInference < Processor
    react_to 'issue.open', 'issue.reopen', 'issue.update',
      'merge_request.open', 'merge_request.reopen', 'merge_request.update'

    # these labels exist in the sytem but are not supported by our groups and stages api
    UNSUPPORTED_DEVOPS_LABELS = [
      'devops::enablement',
      'devops::protect',
      'devops::non_devops'
    ].freeze
    UNSUPPORTED_GROUP_LABELS = [
      'group::not_owned [DEPRECATED]',
      'group::pricing',
      'group::memory',
      'group::container_security',
      'group::incubation',
      'group::testing',
      'group::verify',
      'group::fulfillment'
    ].freeze

    # Applicable when a section::, devops:: or group:: label is present/changed
    # @return [Boolean] true when
    def applicable?
      event.from_gitlab_org? &&
        !event.gitlab_bot_event_actor? &&
        event.label_names.any? { |l| l.match?(/section::.+|devops::.+|group::.+/) }
    end

    def process
      @new_labels = {}

      validate_labels

      add_comment(comment, append_source_link: false) if @new_labels.any?
    end

    def documentation
      <<~TEXT
        Section, Stage and Group labels must align.

        Group will always take priority over Stage and Section labels. This will allow for the least amount of
        changes. I.e., when a Group label is added/updated, Stage and Section labels are updated.

        - When no Group Stage or Section labels exist: Do nothing
        - When only a Section label is present: Do nothing
        - When only a Group label is present: Infer Stage and Section label based on the Group
        - When only a Stage label is present: Infer Section label based on the Stage
        - When only Stage and Group labels are present
          - If the Stage and Group labels match: Only infer the Section label
          - If the Stage and Group labels mismatch: Change the Stage and Section labels based on the Group
        - When only Section and Group labels are present
          - If the Section and Group labels match: Only infer the Stage label
          - If the Section and Group labels mismatch: Change the Section and Stage labels based on the Group
        - When all Section, Stage and Group labels are present
          - If the Section does not match the Group: Change the Section label based on the Group
          - If the Stage does not match the Group: Change the Stage label based on the Group

        Examples:
          Good:
            "section::dev" "devops::create" "group::source code"
            "section::dev" "devops::plan" "group::editor"

          Bad:
            "section::ops" "devops::create" "group::source code" #=> Problem: Incorrect Section; Action: change section::ops to section::dev
            "devops::create" "group::source code" #=> Problem: No Section; Action: add section::dev
            "section::ops" #=> Problem: Missing Stage and Group labels; Action: Leave a note for adding Stage and Group labels
            "section::dev" "devops::create" #=> Problem: Missing Group label; Action: Do nothing.
            "devops::plan" "group::editor" => Problem: No Section, Incorrect Stage; Action: add section::dev, change devops::plan to devops::create

        Note: This processor depends on a valid Enterprise License to support Scoped Labels.  If no EE
              license is present, you may receive two identical comments during an update.
      TEXT
    end

    private

    def label_names
      @label_names ||= event.label_names
    end

    def stages_from_api
      @stages_from_api ||= WwwGitLabCom.stages
    end

    def groups_from_api
      @groups_from_api ||= WwwGitLabCom.groups
    end

    def validate_labels
      validate_group_label if group_label
      validate_devops_label if devops_label
      validate_section_label if section_label
    end

    # Validates the ~group:: label
    def validate_group_label
      infer_section_from_group unless section_label
      infer_devops_from_group unless devops_label

      # validate that the existing devops label applies to the group. If not, change it
      infer_devops_from_group if expected_devops_label && devops_label != expected_devops_label
    end

    # Validates the ~devops:: label
    def validate_devops_label
      infer_section_from_devops if expected_section_label && section_label != expected_section_label
    end

    # Validates the ~section:: label
    def validate_section_label
      return unless validated_stage_name_from_devops_label && validated_group_name

      infer_section_from_group if expected_section_label && section_label != expected_section_label
    end

    # Given a Group label, infer the ~devops:: label
    def infer_devops_from_group
      @new_labels[expected_devops_label] = group_label
    end

    # Given a Group label, infer the ~section:: label
    def infer_section_from_group
      @new_labels[expected_section_label] = group_label
    end

    def infer_section_from_devops
      @new_labels[expected_section_label] = devops_label
    end

    # @return [String,nil] the full name of the group label, excluding unsupported labels
    # @see #validated_group_name to return the label name (without the scope)
    def group_label
      label_names.find do |label_name|
        label_name.start_with?('group::') && !UNSUPPORTED_GROUP_LABELS.include?(label_name)
      end
    end

    # @return [String,nil] the full name of the devops label, excluding unsupported labels
    # @see #validated_stage_name_from_devops_label to return the label name (without the scope)
    def devops_label
      label_names.find do |label_name|
        label_name.start_with?('devops::') && !UNSUPPORTED_DEVOPS_LABELS.include?(label_name)
      end
    end

    # @return [String,nil] the full name of the section label
    def section_label
      label_names.find { |l| l.start_with?('section::') }
    end

    def validated_group_name
      return unless group_label

      raw_group_name = group_label&.split('::')&.last
      parsed_group_name = parse_group_name(raw_group_name)

      raise ArgumentError, "#{parsed_group_name} group is not found in groups api response." if groups_from_api[parsed_group_name].nil?

      parsed_group_name
    end

    def parse_group_name(raw_group_name)
      if raw_group_name == 'distribution'
        # A subgroup label such as Distribution:Build, Distribution:Deploy can be used to identify the specific group
        distribution_subgroup_label = label_names.find { |label| label.include?('Distribution:') }
        distribution_subgroup_label.nil? ? 'distribution_build' : distribution_subgroup_label.downcase.tr(':', '_')
      else
        raw_group_name&.tr(' ', '_')
      end
    end

    def validated_stage_name_from_devops_label
      return unless devops_label

      devops_name = devops_label&.split('::')&.last

      raise ArgumentError, "#{devops_name} stage is not found in stages api response." if stages_from_api[devops_name].nil?

      devops_name
    end

    # Based on the ~group:: label, determine what the appropriate ~devops:: label should be
    def expected_devops_label
      stages_from_api[groups_from_api[validated_group_name]['stage']]['label'] if validated_group_name
    end

    # Based on the ~devops:: label, determine what the appropriate ~section:: label should be
    # @note Group takes precedence over the Stage label
    def expected_section_label
      return "section::#{groups_from_api[validated_group_name]['section']}" if validated_group_name

      "section::#{stages_from_api[validated_stage_name_from_devops_label]['section']}" if validated_stage_name_from_devops_label
    end

    # Return a GitLab-formatted label
    # @return [String] GitLab-formatted label markdown
    # @example
    #   markdown_label('test') #=> %(~"test")
    #   markdown_label('scoped::label') #=> %(~"scoped::label")
    def markdown_label(label)
      %(~"#{label}")
    end

    # Markdown comment to be left on the Issuable detailing what changes were made
    # and why
    # @return [String] GitLab-friendly Markdown
    def comment
      quick_action_labels = @new_labels.keys.map { |label| markdown_label(label) }

      <<~MARKDOWN.chomp
        /label #{quick_action_labels.join(' ')}
      MARKDOWN
    end
  end
end
