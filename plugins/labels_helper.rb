# frozen_string_literal: true

require_relative '../lib/labels_helper'

Gitlab::Triage::Resource::Context.include LabelsHelper
